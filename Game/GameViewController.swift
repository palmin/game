//
//  GameViewController.swift
//  BitGame
//
//  Created by Anders Borum on 15/11/2017.
//  Copyright © 2017 Working Copy. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var gameView: SKView!
    @IBOutlet weak var keychoiceView: UIView!
    @IBOutlet weak var titleView: UIView!

    @IBOutlet weak var tabKeyView: UIView!
    @IBOutlet weak var spaceKeyView: UIView!
    
    @IBOutlet weak var subheadLabel: AnimatedLabel!
    @IBOutlet weak var subheadShadow: AnimatedLabel!
    
    var tabPosition = [0.0, 0.0]
    var spacePosition = [0.0, 0.0]

    var gameScene: GameScene!
    
    func pickHero(name: String) {
        UIView.animate(withDuration: 0.4, animations: {
            self.gameScene.pickHero(name: name)
            self.gameView.alpha = 1
            self.keychoiceView.alpha = 0
            self.titleView.alpha = 0
            self.subheadLabel.alpha = 0
            self.subheadShadow.alpha = 0
        })
    }
    
    @IBAction func tabsPicked(_ sender: Any) {
        pickHero(name: "tab")
    }
    
    @IBAction func spacesPicked(_ sender: Any) {
        pickHero(name: "space")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        float(view: tabKeyView, speed: 2.0)
        float(view: spaceKeyView, speed: 1.0)
    }
    
    func float(view: UIView, speed: Double) {
        let delay = 1.0 / speed
        let duration = delay + delay * drand48()
        let dx = CGFloat(-10.0 + 20.0 * drand48())
        let dy = CGFloat(-5.0 + 10.0 * drand48())
        
        UIView.animate(withDuration: duration, animations: {
           
            view.transform = CGAffineTransform(translationX: dx, y: dy)
            
        }, completion: { completed in self.float(view: view, speed: speed) })
    }
    
    func restart() {
        UIView.animate(withDuration: 1.2, animations: {

            // hide game
            self.gameView.alpha = 0

        }, completion: { completed in
            
            // reset game state
            self.setupScene()
            
            // show choice of keys again
            UIView.animate(withDuration: 1.0, animations: {
                self.keychoiceView.alpha = 1
                self.titleView.alpha = 1
                self.subheadLabel.alpha = 1
                self.subheadShadow.alpha = 1
            })
        })
    }
    
    func setupScene() {
        guard let scene = GKScene(fileNamed: "GameScene") else { return }
        guard let sceneNode = scene.rootNode as! GameScene? else { return }
        
        gameScene = sceneNode
        sceneNode.heroPicked = {
            self.inputField.becomeFirstResponder()
        }
        sceneNode.playerLost = {
            self.becomeFirstResponder()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
                self.restart()
            }
        }
                
        // Copy gameplay related content over to the scene
        sceneNode.entities = scene.entities
        sceneNode.graphs = scene.graphs
        sceneNode.backgroundColor = UIColor.clear
                
        // Set the scale mode to scale to fit the window
        sceneNode.scaleMode = .aspectFill
                
        // Present the scene, but do not show yet
        let view = self.gameView!
        view.presentScene(sceneNode)
        view.allowsTransparency = true
        view.ignoresSiblingOrder = true
        
        UIView.performWithoutAnimation {
            view.isHidden = false
            view.alpha = 0
        }
        
        //view.showsFPS = true
        //view.showsNodeCount = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputField.delegate = self
        
        // listen for keyboard changes
        NotificationCenter.default.addObserver(self, selector: #selector(kbdChanged),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)

        setupScene()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var canBecomeFirstResponder: Bool { return true }
    
    // MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.count == 1 {
            if let char = string.lowercased().first {
                gameScene.typed(character: char)
            }
        }
        
        return gameScene.gameRunning
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return !gameScene.gameRunning
    }
    
    // MARK: Keyboard
    
    @objc func kbdChanged(notification: NSNotification) {
        var top = self.view.bounds.size.height
        var duration = 0.25
        
        if let frame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            top = view.convert(frame, to: view.window).origin.y
        }
        
        if let value = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
            duration = value.doubleValue
        }
        
        guard let scene = gameScene else { return }
        scene.setKeyboard(top: top, duration: duration)
    }
}
