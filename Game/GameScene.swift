//
//  GameScene.swift
//  BitGame
//
//  Created by Anders Borum on 15/11/2017.
//  Copyright © 2017 Working Copy. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var entities = [GKEntity]()
    var words = [WordNode]()
    
    var graphs = [String : GKGraph]()
    
    private var hero: HeroNode?
    
    private var lastUpdateTime : TimeInterval = 0
    private var label : SKLabelNode?
    
    var sourceIndex = 0
    var source = [String]()
        
    override func sceneDidLoad() {
        
        let url = Bundle.main.url(forResource: "words", withExtension: "txt")!
        let text = try! String.init(contentsOf: url)

        for token in text.split(separator: " ") {
            
            source.append(String(token))
        }
        
        Laser.prewarm()
        
        self.lastUpdateTime = 0
        
        // Get label node from scene and store it for use later
        self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
        if let label = self.label {
            label.alpha = 0.0
            label.run(SKAction.fadeIn(withDuration: 2.0))
        }
    }
    
    func addWord(_ text: String) {
        let node = WordNode(text)
        let size = frame.size
        let nodeSize = node.calculateAccumulatedFrame().size
        let availWidth = size.width - nodeSize.width
        let rand = CGFloat(drand48() - 0.5)
        let x = rand * availWidth
        node.position = CGPoint(x: x, y: 0.5 * size.height - 1.5 * nodeSize.height)
        
        node.velocity = currentVelocity
        NSLog("\(text) added with velocity \(node.velocity)")
        
        words.append(node)
        self.addChild(node)
    }
    
    func addNextWord() -> String {
        if sourceIndex >= source.count {
            sourceIndex = 0
        }
        
        let token = source[sourceIndex]
        addWord(token)
        sourceIndex += 1
        return token
    }
    
    public func typed(character: Character) {
        guard gameRunning else { return }
        
        var index = 0
        for word in words {
            guard let point = word.typed(character: character) else { continue }
            
            let laser = Laser.between(hero!.position, point)
            addChild(laser)
            
            if word.isDone {
                words.remove(at: index)

                word.run(SKAction.fadeOut(withDuration: 1.0), completion: {
                    word.removeFromParent()
                })
            }
            index += 1
        }
    }
    
    public var heroPicked : () -> () = {}
    
    func pickHero(_ node: HeroNode) {
        hero = node
        heroPicked()
        
        // fade out the other heroes
        for node in children {
            if node != hero && node is HeroNode {
                node.run(SKAction.fadeOut(withDuration: 0.5))
            }
        }
    }
    
    func placeHero(duration: TimeInterval) {
        guard let node = hero else { return }
        
        let pt = convertPoint(fromView: CGPoint(x: 0, y: kbdTop))
        let y = pt.y + 0.5 * node.frame.size.height
        node.run(SKAction.moveTo(y: y, duration: duration))
    }
    
    public func pickHero(name: String) {
        guard hero == nil else { return }
        
        for node in children {
            if node is HeroNode && node.name == name {
                pickHero(node as! HeroNode)
            }
        }
    }
    
    private var whenLastWordAdded: TimeInterval = 0
    private var lastWordAdded = ""
    private var gameStart: TimeInterval = 0
    private var gameIsLost = false
    
    private var gameDuration : TimeInterval {
        return lastUpdateTime - gameStart
    }
    
    private var timeBetweenCharacters : TimeInterval {
        return 0.6 * exp(-0.01 * gameDuration)
    }
    
    private var currentVelocity : TimeInterval {
        return 200.0 + 150.0 * log(1.0 + 0.001 * gameDuration)
    }
    
    public var gameRunning : Bool {
        return hero != nil && !gameIsLost
    }
    
    override func update(_ currentTime: TimeInterval) {
        guard gameRunning else { return }

        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            gameStart = currentTime
            self.lastUpdateTime = currentTime
        }
        
        let timeSinceLastWord = currentTime - whenLastWordAdded
        if timeSinceLastWord > timeBetweenCharacters * Double(lastWordAdded.count) {
            NSLog("Adding new word as it has been \(timeSinceLastWord) since last word")
            whenLastWordAdded = currentTime
            lastWordAdded = addNextWord()
        }
        
        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
        
        // Update entities
        for entity in self.entities {
            entity.update(deltaTime: dt)
        }
        
        for word in words {
            word.update(deltaTime: dt)
            
            let collision = word.position.y <= hero!.frame.midY
            if collision {
                everythingIsLost()
            }
        }

        self.lastUpdateTime = currentTime
    }
    
    func everythingIsLost() {
        gameIsLost = true
        hero!.run(SKAction.fadeOut(withDuration: 1.0), completion: playerLost)
        
        for word in words {
            word.celebrateWin()
        }
    }
    
    public var playerLost : () -> () = {}
    
    private var kbdTop = CGFloat(0.0)
    public func setKeyboard(top: CGFloat, duration: TimeInterval)  {
        kbdTop = top
        placeHero(duration: duration)
    }
}
