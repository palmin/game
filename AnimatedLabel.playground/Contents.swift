//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport

// Label that can animate the text shown
class AnimatedLabel : UILabel {
    var displayLink: CADisplayLink?
    var startTime: CFTimeInterval?
    var targetText = String()
    
    private func stopAnimation() {
        if let existingLink = displayLink {
            existingLink.remove(from: .current, forMode: .defaultRunLoopMode)
        }
    }
    
    public func animatedSet(text: String, duration: TimeInterval) {
        stopAnimation()
        
        targetText = text
        startTime = nil
        
        let link = CADisplayLink(target: self, selector: #selector(step))
        link.add(to: .current, forMode: .defaultRunLoopMode)
        displayLink = link
    }
    
    @objc private func step(displaylink: CADisplayLink) {
        if startTime == nil {
            startTime = displayLink?.timestamp
        }
        
        let seconds = displayLink!.timestamp - startTime!
        let expectedLength = Int(15.0 * seconds)
        
        // label should be fully drawn and we can stop animating
        if expectedLength >= targetText.count {
            text = targetText
            stopAnimation()
        } else {
            let start = targetText.startIndex
            let end = targetText.index(start, offsetBy: expectedLength)
            let substring = targetText[..<end]
            text = String(substring)
        }
    }
}

class MyViewController : UIViewController {
    func addLabel(color: UIColor, origin: CGPoint) {
        let label = AnimatedLabel()
        label.frame = CGRect(x: origin.x, y: origin.y, width: 600, height: 200)
        label.font = UIFont.monospacedDigitSystemFont(ofSize: 32, weight: .bold)
        label.animatedSet(text: "Intergalactic Type Safety", duration: 2.0)
        label.textColor = color
        
        view.addSubview(label)
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        self.view = view
        
        addLabel(color: .black, origin: CGPoint(x: 20-1, y: 100+1))
        let color = UIColor(displayP3Red: 0.7, green: 0.8, blue: 0.07, alpha: 1)
        addLabel(color: color, origin: CGPoint(x: 20, y: 100))
        
    }
}
// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
